# Erstiheft-Editor

## Kurzanleitung

Der Erstiheft-Editor läuft auf dem FSR-Server https://c109-159.cloud.gwdg.de/.

> **Leider ist der Erstiheft-Editor zur Zeit nur mit Chrome-basierten Browsern kompatibel. *Insbesondere funktioniert Mozilla Firefox nicht.***

Auf der Onlineumgebung läuft eine Onlineversion des Editors [Visual Studio Code](https://vscodium.com/). Außerdem sind texlive-full ($\LaTeX$), inkscape (svg konvertieren), python3 und ein paar andere Kleinigkeiten installiert.

Der Erstiheft-Editor ist mit dem Account `fsr` und einem Passwort gesichert, dass im Studip-Wiki des FSR zu finden ist. Du musst das Passwort normalerweise nur einmal eingeben, dein Browser merkt sich das danach.

> **Es gibt nur ein Passwort für alle, sei also besonders vorsichtig. Da das Passwort für händische Eingabe etwas lang ist, empfehlen wir dringend einen Passwortmanager, z.B. https://vault.bitwarden.com/#/register.**

### Workflow - wie man das Erstiheft editiert

> Bitte immer eine eigene Kopie des Erstihefts verwenden und diese in deinen Userordner legen!


1. deinen eigenen Ordner öffnen (ggf. zuerst anlegen)
2. Terminal öffnen und Erstiheft clonen `git clone https://gitlab.gwdg.de/GAUMI-fachschaft/Erstiheft.git`
   > **git mit ssh key zu benutzen, ist in dieser Umgebung leider nicht sicher. Wenn du besonders genervt und/oder experimentierfreudig bist, kannst du das Repo aber mit [Project Access Tokens](https://gitlab.gwdg.de/GAUMI-fachschaft/Erstiheft/-/settings/access_tokens) clonen, dann brauchst du auch kein Passwort.**
3. ganz oben im Editorfenster deine Emailadresse und danach dein Passwort eingeben und warten bis das Erstiheft heruntergeladen ist.
4. (wenn du möchtest) über das Menü das richtige Erstiheft-Verzeichnis öffnen und oder mit dem Terminal in diesen Ordner wechseln `cd Erstiheft`
5. Mit `make` gucken, ob du alles kompilieren kannst :wink:

> **Die Daten auf dem Server werden nicht gesichert, du solltest alles immer so bald wie möglich `commit`en und `push`en.**

### VS Code Plugins

Um Dinge zu tun, kann man auch Plugins von Visual Studio Code installieren. Die meisten Plugins sind kompatibel mit der Servervariante. Aktuell sind folgende Plugins installiert:

- anwar.papyrus-pdf-0.0.7
- efoerster.texlab-4.2.2
- ms-vscode.makefile-tools-0.6.0
- sndst00m.vscode-native-svg-preview-1.60.2
- tomoki1207.pdf-1.1.0
- torn4dom4n.latex-support-3.10.0

Beim Plugin texlab funktioniert die PDF-Vorschau nicht. PDF-Dateien werden deshalb aktuell mit papyrus angezeigt.

## zur Server Wartung
### etwas installieren
Suche dir das passende Ubuntu-Paket heraus und ergänze in der Dockerfile dieses Repos den letzten `apt install` Befehl, indem du den Paketnamen hinten (mit Leertaste getrennt) dazu schreibst.

Führe danach auf dem Server folgende Befehle aus:
```bash
# ins richtige Verzeichnis wechseln
cd erstiheft-editor
# Container neu bauen und neu starten
sudo ./run.sh
```

Da texlive-full viele GB hat, kann dieser Vorgang auch durchaus mal länger als eine halbe Stunde dauern.

### Updates & Wartung
Der Server updatet sich selbst nach dem gleichen Schema wie der [Erstiheft-Pipelineserver](https://pad.gwdg.de/as8kL_Z3QzWRUSkA-ih8OQ#) und ist auch sonst analog konfiguriert:

- name: erstiheft-editor
- (öffentliche) IP-Adresse: 141.5.109.159
- Domain: https://c109-159.cloud.gwdg.de/
- Betriebssystem: Ubuntu 20.04
- Server-Passwort: <siehe StudIP-Wiki></siehe>

Installiert sind:

- docker
- caddy (ein Webserver der automatisch https konfiguriert)
    - siehe die Configdatei `/etc/caddy/Caddyfile`
    - im Repo liegt **eine Kopie** der Caddyfile. Um das System zu ändern muss diese dort zunächst ersetzt werden. Danach muss der Server oder caddy neugestartet werden (`sudo reboot` bzw. `sudo systemctl restart caddy`)
