echo "Always run this script with sudo!"
echo "=================================\n"
echo "stop all instances "
docker kill $(docker ps -q)
echo "rebuild the texide container"
docker build . -t texide
echo "cleaning up old docker containers"
docker image prune -a
echo "start texide container"
docker run --restart unless-stopped -d -it --init -p 3000:3000 -v "$(pwd)/data:/home/workspace:cached" texide
