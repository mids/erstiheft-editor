FROM gitpod/openvscode-server:latest

USER root
RUN apt-get update
RUN apt-get -y install python3 python3-pip ipython3
# Erstiheft
RUN apt-get -y install texlive-full latexmk
#RUN apt-get -y install latexmk texlive-lang-german
RUN apt-get -y install inkscape
USER openvscode-server
ENTRYPOINT [ "/bin/sh", "-c", "exec ${OPENVSCODE_SERVER_ROOT}/bin/openvscode-server --host 0.0.0.0 --telemetry-level off --accept-server-license-terms --without-connection-token \"${@}\"", "--" ]

